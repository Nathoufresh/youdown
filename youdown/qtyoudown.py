from __future__ import unicode_literals
from __future__ import print_function
import shutil
import requests
import subprocess
import json
import lz4.block
import os
import sys
import re
import youtube_dl
import threading
import eyed3
import fnmatch
from pathlib import Path
import platform
from PyQt5 import QtCore, QtWidgets, QtGui


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        global configs
        global logger
        self.setGeometry(600, 250, 800, 300)
        self.setWindowTitle("Youdown")
        self.stackedCentral=QtWidgets.QStackedWidget(self)
        self.centralWidget=QtWidgets.QWidget(self.stackedCentral)
        self.settingsCentralWidget=QtWidgets.QWidget(self.stackedCentral)
        self.logsCentralWidget=QtWidgets.QScrollArea(self.stackedCentral)
        self.logsCentralWidget.setWidgetResizable(True)
        self.stackedCentral.addWidget(self.centralWidget)
        self.stackedCentral.addWidget(self.settingsCentralWidget)
        self.stackedCentral.addWidget(self.logsCentralWidget)
        self.stackedCentral.setCurrentWidget(self.centralWidget)
        self.toolBar=QtWidgets.QToolBar(self)
        self.settingsAction=QtWidgets.QAction("Settings", self)
        self.settingsAction.triggered.connect(self.show_settings)
        self.logsAction=QtWidgets.QAction("Logs", self)
        self.logsAction.triggered.connect(self.show_logs)
        self.toolBar.addAction(self.settingsAction)
        self.toolBar.addAction(self.logsAction)
        self.addToolBar(self.toolBar)

        self.mainVLayout = QtWidgets.QVBoxLayout(self.centralWidget)
        self.buttonHLayout = QtWidgets.QHBoxLayout(self.centralWidget)
        self.tabList = QtWidgets.QTableView(self.centralWidget)
        # check all tabs when title header column is double clicked
        self.tabList.horizontalHeader().sectionDoubleClicked.connect(self.toggleSelectionAll)
        self.tabModel = QtGui.QStandardItemModel()
        self.tabModel.setColumnCount(4)
        self.tabModel.setHorizontalHeaderLabels(["Title", "Begin at", "Format", "Destination"])
        self.tabList.setModel(self.tabModel)
        self.tabList.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
        self.tabList.verticalHeader().hide()
        self.tabList.setShowGrid(False)
        self.tabList.setFocusPolicy(QtCore.Qt.NoFocus)
        # the goal here is to have a qtableview but some of the columns will need to be edited as a widget like a qcombobox.
        # That's why itemDelegate are useful here
        # they are defined in their own class
        self.fileDelegate = FileDelegate()
        self.comboBoxDelegate = ComboBoxDelegate()
        self.timeEditDelegate = TimeEditDelegate()

        self.tabList.setItemDelegateForColumn(1, self.timeEditDelegate)
        self.tabList.setItemDelegateForColumn(2, self.comboBoxDelegate)
        self.tabList.setItemDelegateForColumn(3, self.fileDelegate)
        self.tabList.selectionModel().selectionChanged.connect(self.selection_changed)
        self.mainVLayout.addWidget(self.tabList)
        self.refreshButton = QtWidgets.QPushButton("Refresh", self.centralWidget)
        self.refreshButton.clicked.connect(self.refresh_ui)
        self.addButton = QtWidgets.QPushButton("Add", self.centralWidget)
        self.addButton.clicked.connect(self.show_add_link_dialog)
        self.downloadButton = QtWidgets.QPushButton("Download", self.centralWidget)
        self.downloadButton.clicked.connect(self.download_ui)
        self.downloadButton.setDisabled(True)
        self.buttonHLayout.addWidget(self.refreshButton)
        self.buttonHLayout.addWidget(self.addButton)
        self.buttonHLayout.addWidget(self.downloadButton)
        self.mainVLayout.addLayout(self.buttonHLayout)
        self.setCentralWidget(self.stackedCentral)
        # signal when the row selection changes
        self.tabModel.itemChanged.connect(self.data_changed)

        # Settings window
        self.temp_configs = {}
        self.settingsGridLayout=QtWidgets.QGridLayout(self.settingsCentralWidget)
        self.musicFolderLabel=QtWidgets.QLabel(self.settingsCentralWidget)
        self.musicFolderLabel.setText("Default download folder")
        self.folderButton=QtWidgets.QPushButton(self.settingsCentralWidget)
        self.folderButton.setText(configs["folder"])
        self.folderButton.clicked.connect(self.choose_music_folder)
        self.okButton=QtWidgets.QPushButton(self.settingsCentralWidget)
        self.okButton.setText("Ok")
        self.okButton.clicked.connect(self.apply_settings)
        self.formatBox = QtWidgets.QComboBox(self.settingsCentralWidget)
        formatList = {"audio", "video"}
        self.formatLabel = QtWidgets.QLabel(self.settingsCentralWidget)
        self.formatLabel.setText("Default format")
        self.formatBox.addItems(formatList)
        self.formatBox.setEditable(True)
        self.formatBox.lineEdit().setAlignment(QtCore.Qt.AlignCenter)
        self.formatBox.lineEdit().setReadOnly(True)
        self.formatBox.setCurrentIndex(self.formatBox.findText('audio'))
        self.settingsGridLayout.addWidget(self.musicFolderLabel, 0, 0)
        self.settingsGridLayout.addWidget(self.folderButton, 0, 1)
        self.settingsGridLayout.addWidget(self.formatBox, 1, 1)
        self.settingsGridLayout.addWidget(self.formatLabel, 1, 0)
        self.settingsGridLayout.addWidget(self.okButton, 2, 1)

        # logs window
        self.logFrame = QtWidgets.QFrame(self.logsCentralWidget)
        self.logVLayout = QtWidgets.QVBoxLayout(self.logFrame)
        self.logVLayout.setAlignment(QtCore.Qt.AlignTop)
        self.logsCentralWidget.setWidget(self.logFrame)

        # add link dialog window
        self.addLinkDialog = QtWidgets.QDialog()
        self.addLinkDialog.setGeometry(600, 250, 300, 75)
        self.addLinkDialog.setWindowTitle("Add link")
        self.addLinkGrid = QtWidgets.QGridLayout(self.addLinkDialog)
        self.linkLineEdit = QtWidgets.QLineEdit(self.addLinkDialog)
        self.linkLineEdit.returnPressed.connect(self.add_link)
        self.addLinkGrid.addWidget(self.linkLineEdit)

    def selection_changed(self, obj=False):
        # when this function is called with the signal
        # self.tabModel.itemChanged.connect(self.data_changed)
        # an arg "obj" is passed and I think obj represents the selected rows..
        # anyway if this function is triggered manually then there's not obj and we need to check if at least one youtube tab object is selected/checked or if a row is selected in tablist 
        if obj and obj.indexes():
            self.downloadButton.setEnabled(True)

        else:
            selectedRows = self.tabList.selectionModel().selectedRows()
            if len(selectedRows) > 0:
                self.downloadButton.setEnabled(True)

            else:
                atLeastOneSelected = False
                for t in self.tabs:
                    if t.selected:
                        atLeastOneSelected = True
                        break
                if atLeastOneSelected:
                    self.downloadButton.setEnabled(True)
                else:
                    self.downloadButton.setEnabled(False)

    def show_add_link_dialog(self):
        self.addLinkDialog.show()
        self.linkLineEdit.clear()

    def add_link(self):
        url = self.linkLineEdit.text()
        source = requests.get(url).text
        title = re.search('(?<=<meta name="title" content=").*(?=")', source).group()
        title = title.replace(" ", "_")
        title = title.replace("'", "+")
        new_tab = YoutubeTab(title, url, True)
        self.tabs.append(new_tab)

        self.addLinkDialog.hide()
        self.refresh_ui()

    def data_changed(self, item):
        # this function is triggered when the content of a row change.
        # In some cases I need to modify what was sent by the itemDelegate before displaying it or set the property of the YoutubeTab object.
        # In case I have to modify it before displaying it in the QtableView I need to block the signal otherwise changing the value of the row will re trigger this function
        row = item.row()
        column = item.column()
        if column == 0:
            self.tabs[row].name = item.text()
            state = item.checkState()
            if state:
                self.tabs[row].selected = True
            else:
                self.tabs[row].selected = False
            self.selection_changed()
        elif column == 1:
            # dont know why I have to use hh:mm , cant use mm:ss
            time = QtCore.QTime.fromString(item.text(), "hh:mm:ss.zzz").toString("hh:mm")
            self.tabModel.blockSignals(True)
            item.setText(time)
            self.tabModel.blockSignals(False)
            self.tabs[row].beginning = item.text()
        elif column == 2:
            self.tabs[row].file_format = item.text()
        elif column == 3:
            folder = item.text()
            display_folder = folder.split("/")[-1]
            self.tabModel.blockSignals(True)
            self.tabs[row].destination = folder
            # the folder cell contains two values, the name of the folder (to be displayed) and the absolut path of the folder
            item.setData(display_folder, QtCore.Qt.DisplayRole)
            item.setData(folder, QtCore.Qt.UserRole)
            self.tabModel.blockSignals(False)

    # select all youtube tabs then reload ui
    def toggleSelectionAll(self, index):
        allSelected = True
        if index == 0:
            for tab in self.tabs:
                if not tab.selected:
                    allSelected = False
                    break

            for tab in self.tabs:
                tab.selected = (not allSelected)

            self.tabModel.blockSignals(True)
            self.load_tab_ui(self.tabs)
            self.tabModel.blockSignals(False)

    def load_tab_ui(self, tabs):
        self.tabModel.clear()
        self.tabModel.setHorizontalHeaderLabels(["Title", "Begin at", "Format", "Destination"])
        for n, t in enumerate(tabs):
            checkbox_and_name = QtGui.QStandardItem()
            checkbox_and_name.setCheckable(True)
            checkbox_and_name.setEditable(True)
            checkbox_and_name.setText(t.name)
            if t.selected:
                # checkstate is 1 middle checked and 2 is checked
                checkbox_and_name.setCheckState(2)
            else:
                checkbox_and_name.setCheckState(0)
            self.tabModel.setItem(n, 0, checkbox_and_name)
            time_beginning_item = QtGui.QStandardItem(t.beginning)
            time_beginning_item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.tabModel.setItem(n, 1, time_beginning_item)
            format_item = QtGui.QStandardItem(t.file_format)
            format_item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.tabModel.setItem(n, 2, format_item)
            folder_item = QtGui.QStandardItem()
            folder_item.setData(t.destination, QtCore.Qt.UserRole)
            folder_item.setData(t.destination.split("/")[-1], QtCore.Qt.DisplayRole)
            folder_item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.tabModel.setItem(n, 3, folder_item)

        self.tabList.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.tabList.horizontalHeader().setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        self.tabList.verticalHeader().setSectionResizeMode(QtWidgets.QHeaderView.ResizeToContents)
        self.tabList.viewport().update()

    def download_ui(self):
        self.show_logs()
        selectedRows = self.tabList.selectionModel().selectedRows()
        toDownload = []
        for r in self.tabs:
            if r.selected:
                toDownload.append(r)
                r.selected = False
        for r in selectedRows:
            if self.tabs[r.row()] not in toDownload:
                toDownload.append(self.tabs[r.row()])
        threads = []
        for t in toDownload:
            pg = progressBar(t, self.logFrame, self.logVLayout)
            t.progressOutputVar = pg
            threads.append(threading.Thread(target=t.download))
        for t in threads:
            t.start()
        self.refresh_ui()

    def refresh_ui(self):
        global configs
        new_tabs = []
        selected_url = []
        # refresh but keep tabs that are checked
        # we keep url to not have 2 times the same tabs, the checked and the newly discovered one
        for t in self.tabs:
            if t.selected:
                new_tabs.append(t)
                selected_url.append(t.url)

        self.tabs = new_tabs
        new_tabs = load_tabs(configs["recovery"])
        if len(self.tabs) > 0:
            for t in new_tabs:
                if t.url not in selected_url:
                    self.tabs.append(t)
        else:
            for t in new_tabs:
                self.tabs.append(t)

        self.tabModel.blockSignals(True)
        self.load_tab_ui(self.tabs)
        self.tabModel.blockSignals(False)

    def show_settings(self):
        if self.stackedCentral.currentWidget() != self.settingsCentralWidget:
            self.stackedCentral.setCurrentWidget(self.settingsCentralWidget)
            self.formatBox.setCurrentIndex(self.formatBox.findText(configs['format']))
            self.folderButton.setText(configs["folder"])
        else:
            self.stackedCentral.setCurrentWidget(self.centralWidget)

    def choose_music_folder(self):
        fileDialog=QtWidgets.QFileDialog()
        fileDialog.setFileMode(QtWidgets.QFileDialog.DirectoryOnly)
        fileDialog.setDirectory(configs["folder"])
        if fileDialog.exec_():
            try:
                self.folderButton.setText(fileDialog.selectedFiles()[0])
            except:
                pass

    def apply_settings(self):
        configs["folder"] = self.folderButton.text()
        configs["format"] = self.formatBox.currentText()
        config = open(config_file, "w")
        config.write(json.dumps(configs))
        config.close()
        self.refresh_ui()
        self.show_settings()

    def show_logs(self):
        if self.stackedCentral.currentWidget() != self.logsCentralWidget:
            self.stackedCentral.setCurrentWidget(self.logsCentralWidget)
        else:
            self.stackedCentral.setCurrentWidget(self.centralWidget)

class FileDelegate(QtWidgets.QStyledItemDelegate):
    def createEditor(self, parent, option, index):
        self.index = index
        self.parentWidget = QtWidgets.QWidget(parent)
        self.fileDialog = QtWidgets.QFileDialog(self.parentWidget, QtCore.Qt.Window)
        self.fileDialog.setFileMode(QtWidgets.QFileDialog.DirectoryOnly)
        # fix a crash ... DontUseNativeDialog
        self.fileDialog.setOption(QtWidgets.QFileDialog.DontUseNativeDialog)
        self.fileDialog.setModal(True)
        directory = Path(index.data(QtCore.Qt.UserRole))
        self.fileDialog.accepted.connect(self.setData)
        self.fileDialog.rejected.connect(self.close)
        self.fileDialog.setDirectory(str(directory.parent))
        self.fileDialog.show()
        return self.parentWidget

    def setModelData(self, editor, model, index):
        model.setData(self.index, self.parentWidget.children()[0].selectedFiles()[0], QtCore.Qt.DisplayRole)

    def setData(self):
        self.setModelData(self.parentWidget, self.index.model(), self.index)
        self.close()

    def close(self):
        self.closeEditor.emit(self.parentWidget)


class ComboBoxDelegate(QtWidgets.QStyledItemDelegate):
    def createEditor(self, parent, option, index):
        combo = QtWidgets.QComboBox(parent)
        combo.addItems(["audio", "video"])
        return combo


class TimeEditDelegate(QtWidgets.QStyledItemDelegate):
    def createEditor(self, parent, option, index):
        self.timeEdit = QtWidgets.QTimeEdit(parent)
        self.timeEdit.setDisplayFormat("hh:mm")
        return self.timeEdit

class YoutubeTab():
    def __init__(self, name, url, selected=False):
        self.name = name
        self.url = url
        self.beginning = "00:00"
        self.duration = None
        self.destination = configs["folder"]
        self.file_format = configs["format"]
        self.selected = selected
        self.progressOutputVar = None
        self.time_regexp = re.compile(r".*time=(\d+:\d+:\d+.\d+)")

    def change_beginning_time(self, new_time):
        self.beginning = new_time

    def rename(self, newname):
        self.name = newname

    def download(self):
        global working_dir
        os.chdir(working_dir)
        global logger
        if self.file_format == "audio":
            outtmpl = os.path.join(self.name)
            ydl_opts={ 'format' : 'bestaudio/',
                       'outtmpl' : outtmpl,
                       'cachedir' : False,
                       'logger' : YoutubeDLLogger(),
                       # needs a logger object in order to use the progress hook function
                       'progress_hooks' : [self.ydl_progress], }
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                ydl.download([self.url])

            self.name = self.name + ".mp3"

            self.duration = float(subprocess.check_output(["ffprobe",
                                                      "-v",
                                                      "error",
                                                      "-show_entries",
                                                      "format=duration",
                                                      "-of",
                                                      "default=noprint_wrappers=1:nokey=1",
                                                      os.path.join(working_dir, outtmpl)]))

            # might cause some issues because ffmpeg's -ss param can be imprecise
            self.duration -= time_string_to_float(self.beginning)

            # ffmpegOutput = subprocess.Popen(["ffmpeg",
            #                                  "-hide_banner",
            #                                  "-loglevel",
            #                                  "error",
            #                                  "-stats",
            #                                  "-nostdin", "-i",
            #                                  os.path.join(working_dir, outtmpl),
            #                                  "-y", "-vn", "-b:a",
            #                                  "320k", "-ac", "2",
            #                                  "-ar", "44100", "-ss",
            #                                  self.beginning,
            #                                  os.path.join(working_dir, self.name)],
            #                                  bufsize=1,
            #                                  universal_newlines=True,
            #                                  stderr = subprocess.PIPE)

            ffmpegOutput = subprocess.Popen(["ffmpeg",
                                             "-hide_banner",
                                             "-loglevel",
                                             "error",
                                             "-stats",
                                             "-nostdin", "-i",
                                             os.path.join(working_dir, outtmpl),
                                             "-y", "-vn", "-c:a", "libmp3lame", "-qscale:a", "2", "-ac", "2",
                                             "-ss",
                                             self.beginning,
                                             os.path.join(working_dir, self.name)],
                                             bufsize=1,
                                             universal_newlines=True,
                                             stderr = subprocess.PIPE)

            # dont know why ffmpeg's output is stderr and not stdout
            for line in ffmpegOutput.stderr:
                self.ffmpeg_progress(line)

            os.remove(outtmpl)
            audiofile=eyed3.load(self.name) # Tag the new mp3 file
            if audiofile is None:
                print("Tag not available")
            else:
                audiofile.initTag()
                for c, s in enumerate(self.name):
                    if s=="-":
                        a=self.name[:(c)]
                        t=self.name[(c+1):-4] # remove .mp3
                        a=a.replace("+", "'")
                        a=a.replace("_", " ")
                        t=t.replace("+", "'")
                        t=t.replace("_", " ")
                        audiofile.tag.artist=a
                        audiofile.tag.title=t
                        audiofile.tag.save()
                        break

        # videos needs improvements
        elif self.file_format == "video":
            outtmpl = self.name + ".%(ext)s"
            ydl_opts={ 'format' : 'bestvideo+bestaudio/', 'outtmpl' : outtmpl, }
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                ydl.download([self.url])
            for f in os.listdir(working_dir):
                if re.search(self.name, f):
                    self.name = f
                    break
            if self.beginning != "00:00":
                logger.update_logs("Converting to " + self.file_format)
                subprocess.run(["ffmpeg", "-hide_banner",
                                "-loglevel", "panic",
                                "-nostdin", "-i",
                                os.path.join(working_dir, self.name),
                                "-y", "-c:a",
                                "copy", "-c:v",
                                "copy", "-ss",
                                self.beginning,
                                "new" + self.name])

                os.remove(self.name)
                os.rename("new" + self.name, self.name)

        source=os.path.join(working_dir, self.name)
        dest=os.path.join(self.destination, self.name)
        shutil.move(source, dest)

        if self.progressOutputVar is not None:
            self.progressOutputVar.progress = 200

    def ydl_progress(self, data):
        if data['status'] == 'finished':
            p = 100
        elif data['status'] == 'downloading':
            p = data['_percent_str']
            p = p.replace('%','')
        if self.progressOutputVar is not None:
            self.progressOutputVar.progress = int(float(p))

    def ffmpeg_progress(self, data):
        if self.progressOutputVar:
            prog = re.findall(self.time_regexp, data)
            try:
                self.progressOutputVar.progress = int(time_string_to_float(prog[0])*100/self.duration)+100
            except:
                pass


class YoutubeDLLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        pass

    def error(self, msg):
        print(msg)


class progressBar(QtWidgets.QWidget):

    updated = QtCore.pyqtSignal()

    def __init__(self, youtubeTab, centralWidget, container):
        super().__init__()
        self.youtubeTab = youtubeTab
        self.__progress = 0
        self.directory = youtubeTab.destination
        self.container = container
        self.centralWidget = centralWidget
        self.groupBox = QtWidgets.QGroupBox(self.centralWidget)
        self.groupBox.setCheckable(False)
        self.groupBox.setMaximumHeight(60)
        self.groupBox.setMinimumHeight(60)
        self.mainHLayout = QtWidgets.QHBoxLayout(self.groupBox)
        self.removeButton = QtWidgets.QPushButton(self.centralWidget)
        self.removeButton.clicked.connect(self.removeProgressBar)
        self.removeIcon = QtGui.QIcon.fromTheme("remove")
        self.removeButton.setIcon(self.removeIcon)
        self.openFolderButton = QtWidgets.QPushButton(self.centralWidget)
        self.openFolderButton.clicked.connect(self.openFolder)
        self.folderIcon = QtGui.QIcon.fromTheme("folder")
        self.openFolderButton.setIcon(self.folderIcon)
        self.openFolderButton.setDisabled(True)
        self.mainHLayout.addWidget(self.removeButton)
        self.mainHLayout.addWidget(self.openFolderButton)
        self.mainVLayout = QtWidgets.QVBoxLayout()
        self.titleLabel = QtWidgets.QLabel(self.centralWidget)
        self.titleLabel.setText(youtubeTab.name)
        self.progressBar = QtWidgets.QProgressBar(self.centralWidget)
        self.progressBar.setRange(0,200)
        self.progressBar.setTextVisible(True)
        self.mainVLayout.addWidget(self.titleLabel)
        self.mainVLayout.addWidget(self.progressBar)
        self.mainHLayout.addLayout(self.mainVLayout)
        self.container.addWidget(self.groupBox)
        self.progressBar.setValue(0)
        self.updated.connect(self.setPgBarValue)

    @property
    def progress(self):
        return self.__progress

    @progress.setter
    def progress(self, val):
        self.__progress = val
        self.updated.emit()
        # show only the folder button when download has suceeded
        if val == 200:
            self.openFolderButton.setEnabled(True)

    def setPgBarValue(self):
        self.progressBar.setValue(self.__progress)

    def removeProgressBar(self):
        # hacky solution
        self.groupBox.setParent(None)

    def openFolder(self):
        # need a non linux solution
        os.system("xdg-open " + self.directory)

def load_tabs(recovery_file):
    tabList = []
    tabList.clear()
    mozlz4_magic = b'mozLz40\x00'
    try:
        with open(recovery_file, 'rb') as f:
            data = f.read()
    except:
        return tabList
    session_data = json.loads(lz4.block.decompress(data.replace(mozlz4_magic, b'', 1)))
    for windows_idx, window in enumerate(session_data['windows']):
        for tab in window['tabs']:
            current_entry = tab["index"]
            current_tab=tab['entries'][current_entry - 1]
            current_tab_url=current_tab['url']
            if re.search("youtube.com/watch+", current_tab_url):
                title=current_tab['title']
                # title=title[:-10] # Remove ' - Youtube'
                title = title.split(" - YouTube")[0] # Remove ' - Youtube'
                for c, s in enumerate(title): # Easier to modify
                    if s=='-' and title[c+1]==' ' and title[c-1]==' ':
                        title=(title[0 : c-1 : ] + '-' + title[ c+2 : :])
                        break
                title=title.replace(' ', '_')
                title=title.replace("'", "+")
                tabList.append(YoutubeTab(title, current_tab_url) )
            elif re.search("soundcloud.com/+", current_tab_url):
                title=current_tab['title']
                title=title.split("by")[0][:-1] # Remove ' by ... soundcloud'
                for c, s in enumerate(title): # Easier to modify
                    if s=='-' and title[c+1]==' ':
                        title=(title[0 : c-1 : ] + '-' + title[ c+2 : :])
                        break
                title=title.replace(' ', '_')
                title=title.replace("'", "+")
                tabList.append(YoutubeTab(title, current_tab_url))

    return tabList

def search_firefox_file(platform_os):
    firefox_recovery_file=""
    if platform_os=="Windows":
        appdata = os.getenv("APPDATA")
        firefox_folder=os.path.join(appdata, "Mozilla\\Firefox\\Profiles\\")
        for folder in os.listdir(firefox_folder):
            if fnmatch.fnmatch(folder, '*default-release*'):
                firefox_recovery_file=os.path.join(firefox_folder, folder)
            elif fnmatch.fnmatch(folder, '*default*'):
                firefox_recovery_file=os.path.join(firefox_folder, folder)
    elif platform_os=='Linux':
        firefox_folder="/home/" + os.getlogin() + "/.mozilla/firefox"
        for folder in os.listdir(firefox_folder):
            if fnmatch.fnmatch(folder, '*default-release'):
                firefox_recovery_file=os.path.join(firefox_folder, folder)
            elif fnmatch.fnmatch(folder, '*default*'):
                firefox_recovery_file=os.path.join(firefox_folder, folder)

        firefox_folder="/Users/" + os.getlogin() + "/Library/Application Support/Firefox/Profiles/"
        for folder in os.listdir(firefox_folder):
            if fnmatch.fnmatch(folder, '*default-release*'):
                firefox_recovery_file = os.path.join(firefox_folder, folder)
            elif fnmatch.fnmatch(folder, '*default*'):
                firefox_recovery_file=os.path.join(firefox_folder, folder)

    return os.path.join(firefox_recovery_file, "sessionstore-backups/recovery.jsonlz4")

def time_string_to_float(time_string):
    values = time_string.split(':')
    if len(values) == 2:
        return float(values[0])*60 + float(values[1])
    elif len(values) == 3:
        return float(values[0])*3600 + float(values[1])*60 + float(values[2])

def main():
    global configs
    global working_dir
    working_dir = os.path.join(os.getenv("HOME"), ".youdown")
    try:
        os.chdir(working_dir)
    except:
        os.chdir(os.getenv("HOME"))
        os.mkdir(".youdown")
        os.chdir(".youdown")
    global config_file
    config_file = "youdown.json"
    if os.path.isfile(config_file):
        config = open(config_file, "r")
        configs = json.load(config)
        config.close()
    else:
        recovery=search_firefox_file(platform.system())
        default_folder = os.path.expanduser("~")
        configs = {"folder" : default_folder, "format" : "audio", "recovery" : recovery}
        config = open(config_file, "a")
        config.write(json.dumps(configs))
        config.close()

    global app
    
    app = QtWidgets.QApplication(sys.argv)
    ui = MainWindow()
    ui.show()
    ui.tabs = []
    ui.refresh_ui()
    sys.exit(app.exec_())

if __name__== "__main__":
    main()
