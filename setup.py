import setuptools

setuptools.setup(
    name="youdown-Nathoufresh",
    version="0.1",
    author="Nathan Colinet",
    author_email="colinetnathan98@gmail.com",
    url="https://gitlab.com/Nathoufresh/youdown",
    packages=["youdown"],
    scripts=["bin/youdown"],
    python_requires='>=3.6',

)
